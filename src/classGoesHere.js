export class Something {
/**
 * 
 * @param {number} second 
 * @param {number} millisecond 
 */
    constructor(second=0, millisecond=0) {
        this.second = second;
        this.millisecond = millisecond;
    }
    timer() {
        if ((this.millisecond += 10) == 1000) {
            this.millisecond = 0;
            this.second++;
            
        }
    }
}
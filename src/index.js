import { Something } from "./classGoesHere";

let timer = new Something();

let mensagem2 = document.querySelector('.mensagem2');
let showSecond = document.querySelector('.second');
let tamo = document.querySelectorAll('.t');
let startButton = document.querySelector('.gameStart');
let gameBox = document.querySelector('.whenStarting');
let restart = document.querySelector('.gameRestart');
//eat, drink etc
let comer = document.querySelector('.comer');
let barraFome = document.querySelector('.fome');
let beber = document.querySelector('.beber');
let barraSede = document.querySelector('.sede');
let lavar = document.querySelector('.lavar');
let barraHigi = document.querySelector('.higiene');
let passear = document.querySelector('.passear');
let barraSocial = document.querySelector('.social');

startButton.style.display = 'block';

//variables for life points
let fome = 100;
let sede = 100;
let higi = 100;
let social = 100;
//variables for timer
// let millisecond = 0;
// let second = 0;
let cron;
let total;

timer.timer();


//function that makes the bar go down, called reduce code
// because i was rewritting everytime for no reason
function reduceCode(time = 1) {

    fome = fome - time;
    sede = sede - time;
    higi = higi - time;
    social = social - time;
    //the changing value uptdated 
    barraFome.innerHTML = fome + '%';
    barraFome.style.width = fome + 'px';

    barraSede.innerHTML = sede + '%';
    barraSede.style.width = sede + 'px';

    barraHigi.innerHTML = higi + '%';
    barraHigi.style.width = higi + 'px';

    barraSocial.innerHTML = social + '%';
    barraSocial.style.width = social + 'px';

    //the seconds correspond to the total points
    showSecond.innerHTML = timer.second;
}

function gamePlay() {
    //condition in case it arrives to zero
    total = fome + sede + higi + social;
    if (fome <= 0 || sede <= 0 || higi <= 0 || social <= 0) {
        //if he dies we take out the buttons 
        restart.style.display = 'block';
        showSecond.style.display = 'none';
        beber.style.display = 'none';
        comer.style.display = 'none';
        lavar.style.display = 'none';
        passear.style.display = 'none';
        clearInterval(cron);
        tamo.forEach(square => square.classList.replace('red', 'black'));
        return mensagem2.innerHTML = 'Why did I deserve to die... Your score is ' + timer.second;
        // the change of colours show the 'dangerous' state of the tamago
    } else if (total <= 50) {
        tamo.forEach(square => square.classList.replace('orange', 'red'));
        reduceCode();
    } else if (total <= 150) {
        tamo.forEach(square => square.classList.replace('yellow', 'orange'));
        reduceCode();
    } else if (total <= 250) {
        tamo.forEach(square => square.classList.replace('blue', 'yellow'));
        reduceCode();
    } else if (total <= 300) {
        tamo.forEach(square => square.classList.replace('t', 'blue'));
        reduceCode();
    } else {
        reduceCode();
    }
}



//timer that we passed into a class
// function timer() {
//     if ((millisecond += 10) == 1000) {
//         millisecond = 0;
//         second++;
//     }

// }




comer.addEventListener('click', () => {
    //when we click it will add some time to his life
    if (fome <= 100) {
        //create a new variable so it can be possible
        let fome2 = fome++;
        timer.second++
            barraFome.innerHTML = fome2 + '%';
        barraFome.style.width = fome2 + 'px';
        total++;
    }

})

lavar.addEventListener('click', () => {
    //when we click it will add some time to his life
    if (higi <= 100 && higi > 0) {
        //create a new variable so it can be possible
        let higi2 = higi++;
        timer.second++;
        barraHigi.innerHTML = higi2 + '%';
        barraHigi.style.width = higi2 + 'px';
        total++;
    }
})

beber.addEventListener('click', () => {
    //when we click it will add some time to his life
    if (sede <= 100) {
        //create a new variable so it can be possible
        let sede2 = sede++;
        timer.second++;
        barraSede.innerHTML = sede2 + '%';
        barraSede.style.width = sede2 + 'px';
        total++;
    }
})


passear.addEventListener('click', () => {
    //when we click it will add some time to his life

    if (social <= 100) {
        //create a new variable so it can be possible
        let social2 = social++;
        timer.second++;
        barraSocial.innerHTML = social2 + '%';
        barraSocial.style.width = social2 + 'px';
        total++;
    }
})

//hide and show when game starts

function start() {

    if (startButton.style.display === 'block') {
        startButton.style.display = 'none'
        gameBox.style.display = 'block'
            //the game starts after 1s and the bar also decreases second by second
        setInterval(gamePlay, 1000);
        //starts timer
        cron = setInterval(() => { timer.timer(); }, 10);
    }
}

startButton.addEventListener('click', () => {
    start()

})

restart.addEventListener('click', () => {
    location.reload()
})
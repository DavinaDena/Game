
## Video Game Project 

### Tamagotchi

**_So for this project, I decided to do a Tamagotchi to reinforce what I learned with the quiz._**

A Tamagotchi is a virtual pet that you have to take care of. Usually, the milestone is to make it live as long as possible.

To begin I had **3 main things** to do:

1. The personage
2. To decrease is life over time
3. The actions that I can do to increase his lifespan

**For the first point** I decided to use bootstrap to create the personage. Since the tama is made of pixels, I took advantage of the grid to create my tama on HTML.

**For the second point**, to represent his life, I decided to use a bar of life at 100% that steadily decreases over time and, **for the third point**, a button to increase his life points.

To make it more than just a life bar, I decided to separate it into **4 basic needs**

- Eat
- Drink
- Clean and
- Social.

Now the lifespan increased to 400% since we gave four 'needs' to the tama and with it, four buttons.

Since the basic gameplay is to make him live as long as possible we need to add a **counter/timer**. The counter/timer is going to be the main score but not only, everytime we click the buttons to feed him, etc, it will also increase the score.

To make it more lively, I decided to change the color of the tamo in function of how much life he has, I also noted it with the HTML so the user can associate the colours to his life.

The only thing missing now is to make him move which I did with CSS animation (keyframes).

To finish I added the functions start and restart.

___

**Last but not least...**

To have a clear direction and vision throughout the project, at first, I decided to take some minutes to myself to think/write what I thought would be important for the project and how it would unwind.

After having a more or less clear idea, I decided to note all the essential points on GitLab on the section Issues -> Boards.

My maquette was made mostly on a notebook that I've tried to replicate on [Figma](https://www.figma.com/file/B5k3hHfMBaA2WwG25H8D9n/Tama?node-id=0%3A1).

Although I couldn't achieve the final idea, I learned some new functions and got a better understanding (I think) of some older functions.

To finish this long ass ReadMe, I felt the project moved quite quickly, and I didn't felt lost due to the organisation and a clear vision of what should do (also youtube).

**TLTR**
-> I used some functions on JavaScript to make a Tamagotchi and put in practice some tactics to organise a project and make it more efficient.
